//
//  NavigationController.h
//  SingleView
//
//  Created by Luis Hernan Paul on 9/23/12.
//  Copyright (c) 2012 Luis Hernan Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationController : UINavigationController
- (void) pushController: (UIViewController*) controller
         withTransition: (UIViewAnimationTransition) transition;
@end
