//
//  ConfigurationViewController.h
//  SingleView
//
//  Created by Luis Hernan Paul on 9/20/12.
//  Copyright (c) 2012 Luis Hernan Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PagViewController : UIViewController
- (IBAction)goToNextPage:(id)sender;
- (IBAction)goToPrevPage:(id)sender;

@end
