//
//  ConfigurationViewController.m
//  SingleView
//
//  Created by Luis Hernan Paul on 9/20/12.
//  Copyright (c) 2012 Luis Hernan Paul. All rights reserved.
//

#import "PagViewController.h"
#import "NavigationController.h"

@interface PagViewController ()

@end

@implementation PagViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)goToNextPage:(id)sender {
    NSLog(@"Prox");
    UIViewController* pag = [self.storyboard instantiateViewControllerWithIdentifier:@"pagina_2"];
    
    [UIView beginAnimations:@"Curl" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:
     UIViewAnimationTransitionCurlUp
                           forView:self.navigationController.view cache:NO];
    
    
    [self.navigationController pushViewController:pag animated:YES];
    [UIView commitAnimations];

}

- (IBAction)goToPrevPage:(id)sender {
    NSLog(@"Prev");
    UIViewController* pag = [self.storyboard instantiateViewControllerWithIdentifier:@"pagina_1"];
    
    [UIView beginAnimations:@"Curl" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:
     UIViewAnimationTransitionCurlDown
                           forView:self.navigationController.view cache:NO];
    
    
    [self.navigationController pushViewController:pag animated:YES];
    [UIView commitAnimations];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
