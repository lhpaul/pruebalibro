//
//  ViewController.h
//  SingleView
//
//  Created by Luis Hernan Paul on 9/7/12.
//  Copyright (c) 2012 Luis Hernan Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ViewController : UIViewController
{
    MPMoviePlayerController *player;
}
@property (strong, nonatomic) IBOutlet UIView *mainView;
@end
