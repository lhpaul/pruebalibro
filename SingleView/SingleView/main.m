//
//  main.m
//  SingleView
//
//  Created by Luis Hernan Paul on 9/7/12.
//  Copyright (c) 2012 Luis Hernan Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
