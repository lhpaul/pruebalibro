//
//  ViewController.m
//  SingleView
//
//  Created by Luis Hernan Paul on 9/7/12.
//  Copyright (c) 2012 Luis Hernan Paul. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize mainView;

- (void)viewDidLoad
{
    UITapGestureRecognizer *TapIdentifier =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneFingerTap)];
    
    // Set required taps and number of touches
    [TapIdentifier setNumberOfTapsRequired:2];
    [TapIdentifier setNumberOfTouchesRequired:1];
    
    
    NSString *url = [[NSBundle mainBundle]
                     pathForResource:@"ipad"
                     ofType:@"m4v"];
    
    player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:url]];
    player.controlStyle = MPMovieControlStyleNone;
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(movieFinishedCallback:)
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:player];
    
    [player prepareToPlay];
    [player.view setFrame: mainView.bounds];  // player's frame must match parent's
    
    UIView *aView = [[UIView alloc] initWithFrame:player.view.bounds];
    [aView addGestureRecognizer:TapIdentifier];
    [player.view addSubview:aView];
    
    [mainView addSubview: player.view];
    [player play];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void) movieFinishedCallback:(NSNotification*) aNotification {
    
    NSLog(@"Termino el video");
    [self terminarVideo];
}

- (void) oneFingerTap
{
    NSLog(@"TAP");
    [self terminarVideo];
}
- (void) terminarVideo
{
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object: player];
    [player.view removeFromSuperview];
    [self performSegueWithIdentifier:@"tercer" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PickGame"])
	{
		
	}
}

- (void)viewDidUnload
{
    [self setMainView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
