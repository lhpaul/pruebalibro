//
//  AppDelegate.h
//  SingleView
//
//  Created by Luis Hernan Paul on 9/7/12.
//  Copyright (c) 2012 Luis Hernan Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
