//
//  SingleViewTests.m
//  SingleViewTests
//
//  Created by Luis Hernan Paul on 9/7/12.
//  Copyright (c) 2012 Luis Hernan Paul. All rights reserved.
//

#import "SingleViewTests.h"

@implementation SingleViewTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in SingleViewTests");
}

@end
